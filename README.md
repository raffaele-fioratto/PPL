# Principles of Programming Languages - Exercise Repo
A collection of exercises collected from the Web that might be useful to study for the exam.

## Scheme (Racket)
* L-99: Ninety-Nine Lisp Problems
    - original page:
<http://www.ic.unicamp.br/~meidanis/courses/mc336/2006s2/funcional/L-99_Ninety-Nine_Lisp_Problems.html>
    - solutions: <https://github.com/dparpyani/99-Lisp-Problems-in-Racket>

## Haskell
* H-99: Ninety-Nine Haskell Problems
    - problem list: <https://wiki.haskell.org/H-99:_Ninety-Nine_Haskell_Problems>
    - solutions: <https://wiki.haskell.org/99_questions/Solutions>

## Erlang
I will reuse the Ninety-Nine Lisp Problems.

Some of them have already solved here: <https://bitbucket.org/m00nlight/99-problems>
