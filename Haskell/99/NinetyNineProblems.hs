module NinetyNineProblems where

import System.Random

-- 1)
myLast :: [a] -> a
myLast [] = error "Empty list!"
myLast [x] = x
myLast (_:xs) = myLast xs

-- 2)
myButLast :: [a] -> a
myButLast [] = error "Empty list!"
myButLast [x, _] = x
myButLast (_:xs) = myButLast xs

-- 3)
elementAt :: [a] -> Int -> a
elementAt (x:_) 1 = x
elementAt (_:xs) i = elementAt xs (i - 1)
elementAt _ _ = error "Index out of bounds"

-- 4)
myLength :: [a] -> Int
myLength [] = 0
myLength (_:xs) = 1 + myLength xs

-- 5)
myReverse :: [a] -> [a]
myReverse [] = []
myReverse (x:xs) = myReverse xs ++ [x]

-- 6)
isPalindrome [] = True
isPalindrome list = list == (reverse list)

-- 7)
data NestedList a = Elem a | List [NestedList a]
myFlatten (Elem a) = [a]
myFlatten (List (x:xs)) = myFlatten x ++ myFlatten (List xs)
myFlatten (List [])  = []

-- 8)
compress [x] = [x]
compress (x:xs) = (if x /= (head xs) then [x] else []) ++ compress xs

-- 9)
pack :: Eq a => [a] -> [[a]]
pack list = foldl packHelper [] list
    where
        packHelper x y
            | (length x) == 0 = x ++ [[y]]
            | ((head (last x)) == y) = (init x) ++ [(last x) ++ [y]]
            | otherwise = x ++ [[y]]

-- 10)
encode list = map (\x -> (length x, head x)) (pack list)

-- 11)
data ListItem a = Single a | Multiple Int a
    deriving (Show)

-- CAVEAT: specify signature otherwise errors
encodeModified :: Eq a => [a] -> [ListItem a]
encodeModified list = map encodeHelper (encode list)
    where
      encodeHelper (1,x) = Single x
      encodeHelper (n,x) = Multiple n x

-- 12)
decodeModified :: [ListItem a] -> [a]
decodeModified = concatMap decodeHelper
    where
        decodeHelper (Single x) = [x]
        decodeHelper (Multiple n x) = replicate n x

-- 13)
encode' :: Eq a => [a] -> [(Int,a)]
encode' = foldr helper []
    where
      helper x [] = [(1,x)]
      helper x (y@(a,b):ys)
        | x == b    = (1+a,x):ys
        | otherwise = (1,x):y:ys

encodeDirect :: Eq a => [a] -> [ListItem a]
encodeDirect = map encodeHelper . encode'
    where
      encodeHelper (1,x) = Single x
      encodeHelper (n,x) = Multiple n x

-- 14)
dupli :: [a] -> [a]
dupli = foldl (\x y -> x ++ [y, y]) []

-- 15)
repli :: [a] -> Int -> [a]
repli list n = foldl (\x y -> x ++ replicate n y) [] list

-- 16)
dropEvery :: [a] -> Int -> [a]
dropEvery xs n = helper xs n
    where helper [] _ = []
          helper (x:xs) 1 = helper xs n
          helper (x:xs) k = x : helper xs (k-1)

-- 17)
split list n = (take n list, drop n list)

-- 18)
slice list i k = take (k - i + 1) (drop (i - 1) list)

-- 19)
rotate list i
    | i == 0 = error "error: i == 0"
    | i > 0 = drop i list ++ take i list
    | i < 0 = drop (length list + i) list ++ take (length list + i) list

-- 20)
removeAt :: Int -> [a] -> (a, [a])
removeAt i list
    | i <= 0 || i > (length list) = error "Out of bounds"
    | otherwise = (list !! (i - 1), take (i - 1) list ++ drop i list)

-- 21)
insertAt :: a -> [a] -> Int -> [a]
insertAt x list i
    | i <= 0 || i > (length list) = error "Out of bounds"
    | otherwise = take (i - 1) list ++ [x] ++ drop (i - 1) list

-- 22)
range :: Int -> Int -> [Int]
range i j
    | i > j = error "i > j"
    | otherwise = rangeHelper i j []
    where
        rangeHelper i j acc
            | i > j = acc
            | i <= j = rangeHelper (i + 1) j (acc ++ [i])

-- 23)
rnd_select :: [a] -> Int -> IO [a]
rnd_select [] _ = return []
rnd_select xs n = do
    gen <- newStdGen
    return $ take n [ xs !! x | x <- randomRs (0, (length xs) - 1) gen]

-- 24)
diff_select n m = diff_select_helper n (range 1 m) []
    where
        diff_select_helper 0 list acc = return acc
        diff_select_helper n list acc = do
            idx <- randomRIO (0, (length list) - 1)
            diff_select_helper (n - 1) ((take idx list) ++ (drop (idx + 1) list)) (acc ++ [list !! idx])
-- 25)
rnd_permu list = rnd_permu_helper list (length list) []
    where
        rnd_permu_helper [] _ acc = return acc
        rnd_permu_helper _ 0 acc = return acc
        rnd_permu_helper list n acc = do
            idx <- randomRIO (0, n - 1)
            rnd_permu_helper ((take idx list) ++ (drop (idx + 1) list)) (n - 1) (acc ++ [list !! idx])

combinations :: Int -> [a] -> [[a]]
combinations 0 _ = [[]]
combinations n xs = [ xs !! i : x | i <- [0..(length xs)-1], x <- combinations (n-1) (drop (i+1) xs) ]
