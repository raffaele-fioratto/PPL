module ESE1_02122016 where

{- exec script:
    ghci
    :l scriptname
-}

-- name of the function :: D -> C
reverse' :: [Int] -> [Int]
reverse' [] = []
-- (x:xs) separate the head of the list with its tail
-- ++ list concatenation
reverse' (x:xs) = reverse' xs ++ [x]

{- write lists
    * [1, 2, 3]
    * [1, 3, ... 30] define a different step
-}

-- note with [Int] -> [Int], with list of floats doesn't work. Change to [a] -> [a] to make it work
-- =========================== --

empty :: [a] -> Bool
empty [] = True
empty (_:xs) = False

-- NO PATTERN MATCHING
range :: Int -> Int -> [Int]
range a b = if a > b
    then error "Low > High"
    else if a < b
        then a : range (a + 1) b
        else [a]

better_range :: Int -> Int -> [Int]
better_range a b
    | a > b  = error "Low > High"
    | a == b = [a]
    | a < b  = a : better_range (a + 1) b

inf_range :: Int -> [Int]
inf_range a = a : inf_range (a + 1)

-- take 10 (inf_range 1)
-- [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

foldl' :: (a -> a -> a) -> a -> [a] -> a
foldl' _ x [] = x
foldl' f x (y:ys) = foldl' f (f x y) ys

-- foldl' (\ x y -> x + y) 0 [1, 2, 3]
-- 6

zip' :: [a] -> [b] -> [(a,b)]
zip' l [] = []
zip' [] l = []
zip' (x:xs) (y:ys) = (x,y):zip' xs ys

triangles :: [(Int, Int, Int)] -- no input
triangles = [(a, b, c) | c <- [1..10], b <- [1..c], a <- [1..b],  a^2 + b^2 == c^2] -- ,  == and

triangles_inf :: [(Int, Int, Int)] -- no input
triangles_inf = [(a, b, c) | c <- [1..], b <- [1..c], a <- [1..b],  a^2 + b^2 == c^2] -- ,  == and

fact' :: Int -> Int
fact' 0 = 1
fact' a = a * fact' (a - 1)

fact :: Integer -> Integer -- Integer => Big Integers
fact n = product [1..n]

-- all elements a are of type RealFloat
bmi :: (RealFloat a) => a -> a -> String
bmi weight height
    | formula < skinny = "Under weight"
    | formula <= ok = "Ok!"
    | formula <= fat = "Fat!"
    | otherwise = "American!"
    where skinny = 18.5
          ok = 25.0
          fat = 30.0
          formula = weight / height ^ 2

quicksort :: (Ord a) => [a] -> [a]
quicksort [] = []
quicksort (x:xs) =
    let left = quicksort [a | a <- xs, a <= x]
        right = quicksort [a | a <- xs, a > x]
    in left ++ [x] ++ right

-- some type
data Boolean = True | False

data TrafficLight = Red | Yellow | Green

instance Eq TrafficLight where
    Red == Red = True
    Yellow == Yellow = True
    Green == Green = True
    _ == _ = False

-- let red = Red
-- show red
-- "Red"
instance Show TrafficLight where
    show Red = "Red"
    show Yellow = "Yellow"
    show Green = "Green"

-- HW: define data structure for bmi -> show, eq (ord)

-- deriving: inherit procedures Show, Read, Eq
data BinaryTree a = EmptyTree | Node a (BinaryTree a) (BinaryTree a) deriving (Show, Read, Eq)

{-
    let empty = EmptyTree
    show empty
->  "EmptyTree"
    let single = Node 10 EmptyTree EmptyTree
    let simpleTree = Node 5 single empty
->  "Node 5 (Node 10 EmptyTree EmptyTree) EmptyTree"
    eq simpleTree == empty
->  False
-}
