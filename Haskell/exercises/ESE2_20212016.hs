module ESE2_20122016

import Control.Applicative
import Control.Monad

data TrafficLight = Red | Yellow | Green

instance Show TrafficLight where
    show Red = 'Red light'
    show Green = 'Green light'
    show Yellow = 'Yellow light'

instance Eq TrafficLight where
    Red == Red = True
    Green == Green = True
    Yellow == Yellow = True
    _ == _ = False

data Point = Point Float Float deriving (Show)

{-
    Point 1.0 1.0
->  Point 1.0 1.0

    Red
->  Red light
-}

getx (Point x _) = x
gety (Point _ y) = y

{-
    let p = Point 42.0 0.42
    getx p
->  42.0
    gety p
->  0.42
-}

data Person = Person {
      name :: String
    , age  :: Int
    , height :: Float
    , weight :: Float
} deriving (Show)

{-
    let r = Person { name = "Guy" , age = 23 , height = 1.8 , weight = 70.0 }
    name r
->  "Guy"
-}

{- make a synonym of Name as String -}
type Name = String
type Age = Int

{- newtype it is more efficient than data -}
data Tree a = Empty | Leaf a | Node (Tree a) (Tree a) deriving (Show)

{- Tree is an ADT, we need to add a in order to make it concrete, but this is not enough we need to ensure that a derives class Eq -}
instance (Eq a) => Eq (Tree a) where
    Empty == Empty = True
    Leaf a == Leaf x = (a == x)
    Node a b == Node x y = (a == x) && (b == y)
    _ == _ = False

{- Foldr for Tree -}
treefoldr :: (t -> a -> a) -> a -> Tree t -> a
treefoldr _ z Empty = z
treefoldr f z (Leaf x) = f x z
treefoldr f z (Node x y) = treefoldr f (treefoldr f z y) x

{-
    let t = Node (Node (Leaf 1) (Leaf 2)) (Node (Leaf 3) (Leaf 4))
    treefoldr (\x y -> (x + y)) 0 t
->  10
-}

instance Foldable Tree where
    foldr = treefoldr

treemap _ Empty = Empty
treemap f (Leaf a) = Leaf $ f a
treemap f (Node x y) = Node (treemap f x) (treemap f y)

{-
    let t = Node (Node (Leaf 1) (Leaf 2)) (Node (Leaf 3) (Leaf 4))
    treemap (\x -> (+) 1 x) t
->  Node (Node (Leaf 2) (Leaf 3)) (Node (Leaf 4) (Leaf 5))
-}

instance Functor Tree where
    fmap = treemap

-- LOGGER
type Log = [String]
newtype Logger a = Logger { run :: (a, Log) }

instance (Show a) => Show (Logger a) where
    show (Logger a) = show a

instance (Eq a) => Eq (Logger a) where
    Logger (x, y) /= Logger (a, b) = (x /= a) || (y /= b)

record :: String -> Logger ()
record s = Logger ((), [s])

{-
    record "Ciao"
->  ((), ["Ciao"])
-}

logmap :: (a -> b) -> Logger a -> Logger b
logmap f lg =
    let (a, l) = run lg
        n = f a
    in Logger(n, l)

instance Functor Logger where
    fmap = logmap

{-
    fmap has to be homomorphic: fmap (f . g) = (fmap f) . (fmap g)
    fmap has to be idempotent to the identity function: fmap id = id
-}

{-
    let l = Logger(1, ["ciao"])
    fmap (\x -> 1 + x) l
->  (2, ["ciao"])
-}

{-
    let l = fmap (*) [1,2,3,4]
    :t l
    l :: Num a => [a -> a]
    fmap (\f (f 9)) l
->  [9,18,27,36]
-}

lapp :: Logger (a -> b) -> Logger a -> Logger b
lapp lf lg =
    let (f, llf) = run lf
        ln = logmap f lg
        (a, l) = run ln
    in Logger (a, l ++ llf)

    instance Applicative Logger where
        pure a = Logger (a, [])
        (<*>) = lapp

{-
    let l = Logger(1, ["ciao"])
    let k = Logger(2, ["miao"])
    let h = Logger(3, [])
    let fl = lapp (*) l
->  Logger(6, ["ciao", "miao"]) TO TEST
-}
