module ESE3_13012017 where

import Control.Applicative
import Control.Monad

type Log = [String]

newtype Logger a = Logger { run :: (a, Log)}

logmap :: (a -> b) -> Logger a -> Logger b
logmap f lg =
    let (a, l) = run lg
        n = f a
    in Logger (n, l)

instance (Show a) => Show (Logger a) where
    show (Logger a) = show a

instance (Eq a) => Eq (Logger a) where
    Logger (x, y) /= Logger (a, b) = (x /= a) || (y /= b)

instance Functor Logger where
    fmap = logmap

lapp :: Logger (a -> b) -> Logger a -> Logger b
lapp lf lg =
    let (f, llf) = run lf
        ln = logmap f lg
        (a, l) = run ln
    in Logger (a, l ++ llf)

instance Applicative Logger where
    pure a = Logger (a, [])
    (<*>) = lapp

instance Monad Logger where
    m >>= f = -- m >>= f :: Logger a -> (a -> Logger b) -> Logger b
        let (a, la) = run m
            mb = f a
            (b, lb) = run mb
        in Logger (b, la ++ lb)

{- monadic laws
*** Left Identity:
    * return a >>= f == f a (minimal context of a is a monad of a)
    * f :: a -> m b

*** Right Identity:
    * m >>= return == m

*** Associativity
    * (m >>= f) >>= g == m >>= (\x -> f x >>= g)
-}

logAddOne :: (Num a) => a -> Logger a
logAddOne a = Logger (a + 1, ["+1"])

{-
    let log = Logger (42, [])
    logAddOne 42
->  (43, ["+1"])
-}

logMultiplyTwo :: (Num a) => a -> Logger a
logMultiplyTwo a = Logger (a * 2, ["*2"])

{-
    let log = Logger (42, [])
    log >>= logAddOne >>= logMultiplyTwo
->  (86, ["+1", "*2"])
-}

logOps :: (Num a) => Logger a -> Logger a
logOps lg = do
    v <- lg -- extract the value from the monad
    p1 <- logAddOne v
    m2 <- logMultiplyTwo p1
    return m2

record :: String -> Logger ()
record s = Logger ((), [s])

try :: (Show a) => a -> Logger([a])
try x = do
    record ("First Op") -- >>, explicit access to state of the Monad
    return [x]

data Tree a = EmptyTree | Node a (Tree a) (Tree a) deriving (Show, Eq)

singletonM :: (Show a) => a -> Logger (Tree a)
singletonM x = do
    record ("Created a singleton " ++ show x)
    return (Node x EmptyTree EmptyTree)

{-
    singletonM 10
->  (Node 10 EmptyTree EmptyTree,["Created a singleton 10"])
-}

treeInsertM :: (Ord a, Show a) => Tree a -> a -> Logger (Tree a)
treeInsertM EmptyTree x = singletonM x
treeInsertM (Node a left right) x
    | x == a = do
        record ("Inserted " ++ show x)
        return (Node x left right)
    | x < a = do
        l <- treeInsertM left x
        return (Node a l right)
    | x > a = do
        r <- treeInsertM right x
        return (Node a left r)

treeFoldr :: (t -> a -> a) -> a -> Tree t -> a
treeFoldr _ z EmptyTree = z
treeFoldr f z (Node b x y) = treeFoldr f (f b (treeFoldr f z y)) x

treeSumM :: (Show a, Num a) => Logger (Tree a) -> Logger a
treeSumM t = do
    v <- fmap (treeFoldr (+) 0) t
    record(" Summed " ++ show v) -- 'show' used here since we are using strings
    return v

{-
    let t = (Node 1 (Node 2 EmptyTree EmptyTree) (Node 3 EmptyTree EmptyTree))
    let l = Logger(t, ["Creation"])
    treeSumM l
->  (6,["Creation"," Summed 6"])
-}

andM :: Logger Bool -> Logger Bool -> Logger Bool
andM l1 l2 = do
    b1 <- l1 -- Extraction of Boolean
    b2 <- l2
    return (b1 && b2)

treeBalanceM :: Tree a -> Logger Bool
treeBalanceM EmptyTree = do
    record "An EmptyTree is always balanced"
    return True
treeBalanceM (Node _ EmptyTree EmptyTree) = do
    record "A single node is always balanced"
    return True
treeBalanceM (Node _ EmptyTree _) = do
    record "Unbalanced"
    return False
treeBalanceM (Node _ _ EmptyTree) = do
    record "Unbalanced"
    return False
treeBalanceM (Node _ left right) = do
    andM (treeBalanceM left) (treeBalanceM right)

{-
    let t = (Node 1 (Node 2 EmptyTree EmptyTree) (Node 3 EmptyTree EmptyTree))
    let t1 = Node 0 EmptyTree t
    treeBalanceM t1
->  (False,["Unbalanced"])
-}
