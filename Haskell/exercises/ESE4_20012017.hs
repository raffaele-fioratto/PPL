module ESE4_20012017 where

import Control.Monad.State

-- STATE MONAD
{-
newtype State s a = State { runState :: s -> (a, s) }

instance Monad State where
    return x = State $ \s -> (x, s)
    m >>= f = State $ \s -> let (a, s') = (runState m) s
                            in runState (f a) s'
-}

type Stack = [Int]

pop :: Stack -> (Int, Stack)
pop (x:xs) = (x, xs)

push :: Int -> Stack -> ((), Stack)
push a xs = ((), a:xs)

stackManip :: Stack -> (Int, Stack)
stackManip stack = let
    (a, stack') = pop stack
    (b, stack'') = pop stack'
    ((), stack''')= push 100 stack''
    in pop stack'''

{-

get :: State s s
get = State $ \s -> (s, s)

put :: s -> State s ()
put s = State $ \_ -> ((), s)

-}


popM :: State Stack Int
popM = do
    h:stack <- get
    put stack
    return h

pushM :: Int -> State Stack ()
pushM a = do
    stack <- get
    put (a:stack)
    return ()

stackManipM :: State Stack Int
stackManipM = do
    popM
    popM
    pushM 100
    popM
