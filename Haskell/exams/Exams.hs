module Exams where

-- pl20160922
transpose :: [[a]] -> [[a]]
transpose [] = []
transpose ls = let
        hs = map head ls
        ts = filter (not . null) $ map tail ls
        in hs : transpose ts

-- pl20160908
checkFig :: (Num a, Eq a) => [[a]] -> Maybe Int
checkFig list = checkFigHelper 0 (length list) list
    where
        checkFigHelper n m list
            | n == m = Just n
            | (list !! n) !! n == 1 = checkFigHelper (n + 1) m list
            | otherwise = Nothing

-- pl20160722
iter f v = [v] ++ iterHelper 1 f v
    where
        iterHelper n f v = [(foldl (.) f (replicate (n - 1) f) v)] ++ (iterHelper (n + 1) f v)

-- pl20160706
data Gtree a = Leaf a | GNode a [Gtree a] deriving (Show, Eq)


-- pl20150922
data Bilist a = Bilist { l1 :: [a], l2 :: [a] } deriving (Show)
bilist_ref :: Bilist a -> Int -> [a]
bilist_ref bl i = [(l1 bl) !! i] ++ [(l2 bl) !! i]

multiIndex :: [a] -> [Int] -> [a]
multiIndex _ [] = []
multiIndex l (ind:inds) = [l !! ind] ++ multiIndex l inds

oddeven :: [a] -> Bilist a
oddeven l = (Bilist (multiIndex l [0,2..(length l) - 1]) (multiIndex l [1,3..(length l)]))

inv_oddeven :: Bilist a -> [a]
inv_oddeven bl = inv_oddevenhelp bl 0 0 where
    inv_oddevenhelp :: Bilist a -> Int -> Int -> [a]
    inv_oddevenhelp bl n s
        | n == length((l1 bl)) || n == length((l2 bl)) = []
        | s == 0 = [(l1 bl) !! n] ++ inv_oddevenhelp bl n 1
        | s == 1 = [(l2 bl) !! n] ++ inv_oddevenhelp bl (n + 1) 0

bilist_max :: (Ord a, Num a) => Bilist a -> Int
bilist_max bl = bilist_max_helper bl 1 0 where
    bilist_max_helper bl cur_idx max_idx
        | cur_idx == length((l1 bl)) || cur_idx == length((l2 bl)) = max_idx
        | (((l1 bl) !! cur_idx) + ((l2 bl) !! cur_idx)) > (((l1 bl) !! max_idx) + ((l2 bl) !! max_idx)) = bilist_max_helper bl (cur_idx + 1) cur_idx
        | otherwise =  bilist_max_helper bl (cur_idx + 1) max_idx
