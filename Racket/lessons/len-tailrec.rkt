#lang racket

(define (len-tr L)
	(len-h L 0))


(define (len-h L n)
	(if (null? L)
		n
		(len-h (cdr L) (+ n 1))))


(len-tr '(1 2 3))
