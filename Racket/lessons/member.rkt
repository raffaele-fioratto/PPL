#lang racket

(define (mem v L)
	(cond 	((null? L) #f)
		((equal? v (car L)) L)
		(else (mem v (cdr L)))))

(mem 2 '(1 2 3))
(mem "hello" '(1 2 3))
; (mem '(1 2 3) "hello")
