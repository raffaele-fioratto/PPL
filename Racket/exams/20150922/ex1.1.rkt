#lang racket

(define (co-sublist L i j)
  (define (co-sublist-aux lst p acc)
    (cond [(empty? lst) acc]
          [(or (< p i) (> p j)) (co-sublist (cdr lst) (+ 1 p) (append acc (car lst)))]
          [else (co-sublist-aux (cdr lst) (+ 1 p) acc)]))
  (co-sublist-aux L 0 '()))