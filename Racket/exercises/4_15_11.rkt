#lang racket

;; Coroutines (much like generators in python)

(define *queue* '())

(define (empty-queue?)
    (null? *queue*))

(define (enqueue e)
    (set! *queue* (append *queue* (list e))))

(define (dequeue)
    (let ((e (car *queue*)))
        (set! *queue* (cdr *queue*))
        e))

(define (make-coroutine proc)
    (call/cc (lambda (cc)
                (enqueue cc) ; save the state
                (proc))))    ; eval proc

(define (yield)
    (call/cc (lambda (cc)
                (enqueue cc)
                ((dequeue)))))

(define (coroutine-exit)
    (if (empty-queue?)
        (exit)
        ((dequeue))))

(define (do-something str reps)
    (lambda ()
        (let loop ((i 0))
            (display str) (display " ") (display i) (newline)
            (yield) ; return to the callee, and execute some other work
            (if (< i reps)
                (loop (+ i 1))
                (coroutine-exit))))) ; a "special" yield

(define (coroutine-test)
    (begin
        (make-coroutine (do-something "Proc A\n" 5))
        (make-coroutine (do-something "Proc B\n" 3))
        (coroutine-exit)))

; Choose - Backtracking
(define *paths* '())

(define (choose choices)
    (if (null? choices)
        (fail)
        (call/cc (lambda (cc)
                    (set! *paths* (cons (lambda ()
                                            (cc (choose (cdr choices)))) ; thunking
                                        *paths*))
                    (car choices)))))

(define fail #f)
(call/cc (lambda (cc)
            (set! fail (lambda () (if (null? *paths*)
                                (cc '!!FAILURE!!)
                                (let ((option (car *paths*)))
                                    (set! *paths* (cdr *paths*))
                                    (option)))))))

(define (is-the-sum-of sum)
    (unless (and (>= sum 0) (<= sum 10))
        (error ("Out of range" sum))
    (let ((x (choose (range 0 5)))
          (y (choose '(0 1 2 3 4 5))))
          (if (= sum (+ x y))
              (list x y)
              (fail)))))

;; Higher order functions

;; interval check

(define (int-check min max L)
    (foldl (lambda (x xs)
                (if (and (> x min) (< x max))
                    (append xs (list x))
                    xs))
            '() L))

(define (^ base exp)
    (let loop ((n 0) (res 1))
        (if (< n exp)
            (loop (+ n 1) (* base res))
            res)))

(define (conv base l)
    (foldr (lambda (exp dig res)
                (+ res (* dig (^ base exp))))
            0 (reverse (range 0 (length l))) l))

(conv 2 '(0))        ; => 0
(conv 2 '(0 1))      ; => 1
(conv 2 '(0 0 0 1))  ; => 1
(conv 5 '(1 3 2))    ; => 42

;; my implementation
(define (composition f g L)
    (map (lambda (x) (f (g x))) L))

(define (o f g)
    (if (and (prodecure? f) (prodecure? g))
        (lambda (args) (f (g args)))
        (error "Not a function")))
