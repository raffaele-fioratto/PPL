#lang racket

;; Closing parenthesis are on one line
;; Each definition (local too) deserves at least one line
;; Naming variable is english words separated with dashes
;; Prefix of a function name is the type of the main argument
;; Suffix of a variable name is its type
;; suffix ? for those predicates which returns a boolean
;; suffix ! for those predicates that are modifiers

(define (hello-world)
    (display "Hello World!!!"))

;; Factorial
(define (fact n)
    (if (= n 0)
        1
        (* n (fact (- n 1)))))

(define TA-name "Lol")

(define (reverse-pair p)
    (if (pair? p)
        (let ((x (car p))
              (y (cdr p)))
            (cons y x))
        (error "Not a pair!")))

(define (life-milestones y)
    (let* ((current-year 2016)
           (age (- current-year y))
           (adult-year (- current-year (- age 18))))
        (display "You are ")
        (display age)
        (display " years old")
        (newline)
        (display "You got 18 in ")
        (display adult-year)
        (newline)))

(define (sum-of-squares l)
    (if (null? l)
        0
        (let ((h (car l)))
            (+ (* h h) (sum-of-squares (cdr l))))))

(define (sos-tail l)
    (define (sos l acc)
        (if (null? l)
            acc
            (sos (cdr l) (+ acc (* (car l) (car l))))))
    (sos l 0))

(define (sos-nl-tail l)
    (let sos ((ls l)
              (acc 0))
        (if (null? ls)
            acc
            (sos (cdr ls) (+ acc (* (car ls) (car ls)))))))

(define (fib n)
    (cond [(= n 1) 1]
          [(= n 2) 1]
          (else (+ (fib (- n 1)) (fib (- n 2))))))

(define (my-map f l)
    (if (null? l)
        '()
        (cons (f (car l)) (my-map f (cdr l)))))

(define (my-map-nl f l)
    (let helper ((ff f)
                 (ll l))
        (if (null? ll)
            '()
            (cons (ff (car ll)) (helper ff (cdr ll))))))

(define (quick-sort l)
    (cond [(or (empty? l) (empty? (cdr l))) l]
          [else (let* ([p (car l)]
                      [s< (filter (lambda (x) (< x p)) (cdr l))]
                      [s> (filter (lambda (x) (>= x p)) (cdr l))])
                (append (quick-sort s<) (list p) 1(quick-sort s>)))]))
