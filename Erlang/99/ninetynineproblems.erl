-module(ninetynineproblems).
-compile(export_all).

% 1)
myLast([]) -> [];
myLast([X|[]]) -> X;
myLast([_|XS]) -> myLast(XS).

% 2)
myButLast([]) -> [];
myButLast([X,_|[]]) -> X;
myButLast([_|XS]) -> myButLast(XS).

% 3)
elementAt([X|_], 1) -> X;
elementAt([_|XS], N) when N > 1 -> elementAt(XS, N - 1).

% 4)
myLength([], Acc) -> Acc;
myLength([_|XS], Acc) -> myLength(XS, Acc + 1).
myLength(L) -> myLength(L, 0).

% 5)
myReverse([]) -> [];
myReverse([X|[]]) -> [X];
myReverse([X|XS]) -> myReverse(XS) ++ [X].

% 6)
isPalindrome(L) -> L =:= myReverse(L).

% 7)
myFlatten([]) -> [];
myFlatten([X|XS]) -> myFlatten(X) ++ myFlatten(XS);
myFlatten(X) -> [X].

% 8)
compress([]) -> [];
compress([X|[]]) -> [X];
compress([X,X|XS]) -> compress([X|XS]);
compress([X,Y|XS]) -> [X] ++ compress([Y|XS]).

% 9)
packHelper([X|[]], Acc) -> {Acc ++ [X], []};
packHelper([X,X|XS], Acc) -> packHelper([X|XS], Acc ++ [X]);
packHelper([X,Y|XS], Acc) -> {Acc ++ [X], [Y|XS]}.

pack([]) -> [];
pack(L) ->
    {Acc, Rest} = packHelper(L, []),
    [Acc] ++ pack(Rest).

% 10)
encode(L) -> lists:foldl(fun([X|XS], AccIn) -> AccIn ++ [{length([X|XS]), X}] end, [], pack(L)).

% 11)
encodeModified(L) -> lists:foldl(fun([X|XS], AccIn) ->
    if
        length([X|XS]) > 1 -> AccIn ++ [{length([X|XS]), X}];
        true -> AccIn ++ [X]
    end
end, [], pack(L)).

% 12)
repeat(_, 0) -> [];
repeat(E, N) -> [E] ++ repeat(E, N - 1).

decodeModified(L) -> lists:foldl(fun(X, AccIn) ->
    if
        is_tuple(X) -> {Length, Elem} = X, AccIn ++ repeat(Elem, Length);
        true -> AccIn ++ [X]
    end
end, [], L).

% 13)
encodeDirect([]) -> [];
encodeDirect([X|XS]) -> SubL = lists:takewhile(fun(E) -> E == X end, XS),
    if
        length(SubL) > 0 -> [{1 + length(SubL), X}] ++ encodeDirect(drop(XS, length(SubL)));
        true -> [X] ++ encodeDirect(XS)
    end.

% 14)
dupli([]) -> [];
dupli([X|XS]) -> [X] ++ [X] ++ dupli(XS).

% 15)
repli([], _) -> [];
repli(L, 0) -> L;
repli([X|XS], N) -> repeat(X, N) ++ repli(XS, N).

% 16)
dropEvery([], _, _) -> [];
dropEvery([_|XS], N, I) when I == N -> dropEvery(XS, N, 1);
dropEvery([X|XS], N, I) when I < N -> [X] ++ dropEvery(XS, N, I + 1).
dropEvery(L, N) -> dropEvery(L, N, 1).

% 17)
take([], _) -> [];
take(_, 0) -> [];
take([X|XS], N) -> [X] ++ take(XS, N - 1).
drop([], _) -> [];
drop(L, 0) -> L;
drop([_|XS], N) -> drop(XS, N - 1).

split([], _) -> [];
split(L, 0) -> L;
split(L, N) -> [take(L, N)] ++ [drop(L, N)].

% 18)
slice([], _, _) -> [];
slice(_, I, J) when I > J -> error("I > J");
slice([X|XS], I, J) when I == 1, J > 1 -> [X] ++ slice(XS, I, J - 1);
slice([X|_], I, J) when I == 1, J == 1 -> [X];
slice([_|XS], I, J) -> slice(XS, I - 1, J - 1).

% 19)
rotate([], _) -> [];
rotate(L, 0) -> L;
rotate(L, N) when N > 0 -> drop(L, N) ++ take(L, N);
rotate(L, N) when N < 0 -> drop(L, length(L) + N) ++ take(L, length(L) + N).

% 20)
removeAt([], _) -> [];
removeAt(_, 0) -> [];
removeAt(L, N) -> {elementAt(L, N), take(L, N - 1) ++ drop(L, N)}.
